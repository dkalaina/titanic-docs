# The titanic cluster documentation 

Welcome on the documentation of the titanic cluster. Here we store everything useful to the use of Titanic,
the cluster maintained by INRIA. It is managed with the SLURM batch scheduler, a lightweight and
efficient way to dispatch jobs on the various nodes of the cluster. A course with exercices is available at: [git@gitlab.inria.fr/ctallec/slurm-tuto](https://gitlab.inria.fr/ctallec/slurm-tuto)


## Summary

1. [Hardware specifications](#hardware-specifications)
2. [Connection to Titanic and running my first job](#connection-to-titanic-and-running-my-first-job)
3. [Send jobs in the optimal way](#send-jobs-in-the-optimal-way)
4. [Docker management](#docker-management)
5. [Job monitoring](#job-monitoring)
6. [Interactive sessions](#interactive-sessions)
7. [Bad practices](#bad-practices)
8. [Reporting issues](#reporting-issues)

## Hardware specifications
- The cluster: titanic, 5 nodes titanic-[1-5], baltic-1, pacific-1, 7 nodes republic-[1-7]
1. For titanic-[1-5]: 48 cpu threads, 4 GTX 1080Ti (12Go)
2. For baltic-1: 48 cpu threads, 4 Titan black GPU (6Go)
3. For pacific-1: 56 cpu threads, 4 P100 GPU (16Go)
4. For republic-[1-7]: 32 CPU threads, 4 RTX 2080Ti (12Go)

- storage: 
1. shared homedir, with high storage capacity: 22To for
all users.
2. nodes all have local storage. If you need additional storage, come and see
us, so that we can create dedicated folders for you on the nodes.
Note: You can always create a symbolic link to your storage directory
on nodes in your homedir! (for me ln -s /data/titanic_3/users/ctallec/data ~/data)
3. In the future, we plan on having a shared directory for usual datasets.
This is not yet implemented.

> [Go to top](#the-titanic-cluster-documentation)

## Connection to titanic and running my first job
### Connection to the cluster:
Use the following procedure
- ssh through host seringa, a ssh platform (`ssh -A example_InriaLDAP@ssh.saclay.inria.fr`)
- then on machine titanic (`ssh -A titanic`)
- you need to forward your ssh key ! (-A)
- you can configure your .ssh/config so that ssh titanic directly connects you to titanic.
  Example:

```
Host inria 
    HostName ssh.saclay.inria.fr
    User example_InriaLDAP
    IdentityFile ~/.ssh/inria_key

Host titnic
    ProxyCommand ssh -q -A inria nc titanic 22 
    User example_InriaLDAP
```

### Enabling cluster account
If you have the `sudo` on titanic-master, you can use the command: 
```bash
sudo sacctmgr add Name=$USER Account=tau Cluster=titanic
```
else come and see the admins!

### Running your first job
Great, you are now ready to send your first job on titanic! 
You can, run this command to check that everything is ok: 

``` bash
srun --pty bash
```
Here we ask slurm for a interactive job (`srun`) and for a prompt (`--pty`).
Normally, your job will be accepted almost immediately. You will have next a
terminal opened on a node ! However, you have not requested any GPU (with the
option `--gres=gpu:1` for example) so you are not given any GPU.
Press `ctrl+C` then `ctrl+D` to exit an interactive job. Do not close your
terminal to exit! Slurm will be not able to tell in some cases that your exited your session.

> [Go to top](#the-titanic-cluster-documentation)

## Send jobs in the optimal way
- To directly launch jobs on nodes on the cluster you need to put them in an executable
  file. Whichever executable file is a valid job to send. For example, launching the following
  executable script:

  ~/script.sh

```bash
  #!/bin/sh

  echo "Bonjour"
```

  Is done with the command
  
```bash
  $ sbatch script.sh
```

- If you need specific cpu ressources, you can specify them. Check SLURM documentation for more
  information on the subject https://slurm.schedmd.com/quickstart.html.
- To use the GPUs, you need to specify the number of GPUs you want. For example, if you want to
  launch the previous script using 2 GPUs, use

```bash
  $ sbatch --gres=gpu:2 script.sh
```

  Alternatively you can specify the requirements for your script (as well as any other options)
  directly in the bash script.

```bash
  #!/bin/sh
  #SBATCH --gres=gpu:2

  echo "Bonjour"

  $ sbatch script.sh
```
  Check out https://slurm.schedmd.com/quickstart.htm://slurm.schedmd.com/quickstart.html for an
  exhaustive list of options.

- If you want a specific type of GPU, use the -C option (short for --constraint=)
  (`pascal` for GTX1080Ti, `kepler` for Titan Black, `tesla` for P100 GPUs,
  and `turing` for RTX2080Ti).

```bash
  $ sbatch --gres=gpu:2 -C pascal script.sh
```

- When GPUs are allocated, you still need to make sur your code is able to run on
  a GPU. Furthermore, allocated GPUs are given by the environment variable $CUDA_VISIBLE_DEVICES.
  Note: Most deeplearning framework do that by default.
  Warning: Never use GPus that were not allocated.

- Queues: Slurm implements a queue system for fair sharing of ressources. Each queue has a different
  allocation behavior. Currently, we have two different queues: a default (priority) queue, and a 
  besteffort queue. 
1. On the default queue, jobs are not killable by other users. If you launch a
   job on the default queue, it will only stop when it has terminated, or if you cancel it by
   yourself. 
2. On the besteffort queue, jobs are killable. Namely, if a user wishes to allocate a job
   on the default queue, while his ressource requirements are not met, but killing a job
   in the besteffort queue would allow his job to run, then the job in the besteffort queue
   is cancelled and requeued. Note that requeuing a job does not preserves its state. You
   need to take care of that yourself.

- Users are only allowed a total of 4 GPUs on the default queue (GPU, not nodes!). If you
  want to launch jobs on more than 4 GPUs, you need to use the besteffort queue.

> [Go to top](#the-titanic-cluster-documentation)

## Docker management

### What is Docker ? 
Docker is an open-source virtual environment manager, much more independent
from the host machine than Anaconda. The cluster will be using Docker for
easier management of all user configurations. In short: it is something that
you activate with your personal folders mounted, to run your calculations.
    
### Easy: Execute custom docker job submissions
3 commands are available to execute from master:
1. sjupyter
2. srun-docker
3. sbatch-docker (not yet available)


Those three are wrappers around srun and sbatch. Check `sbatch-docker --help`
for example to have info on the arguments (check the `scripts` folder of this
git to look at the source code)

1. **sjupyter** runs a jupyter notebook inside a docker, everything being customizable with arguments:
   ```bash
   dkalaina@master:$ sjupyter --gres=gpu:1  --docker_image=divkal/nvidia-tensorflow:1.5
   ```
2. **srun-docker** runs a interactive bash session in a docker:
   ```bash
   dkalaina@master:$ srun-docker --gres=gpu:1 --docker_image=divkal/nvidia-tensorflow:1.5
   ```
3. **sbatch-docker** runs a bash script inside a docker. It accepts the #SBATCH arguments in the script as well. 
   ```bash
   dkalaina@master:$ sbatch-docker --gres=gpu:1 --docker_image=divkal/nvidia-tensorflow:1.5 script.sh
   
   # help of sbatch-docker:
   usage: sbatch-docker [-h] [--docker_image DOCKER_IMAGE] [--workdir WORKDIR]
                    [--docker_args DOCKER_ARGS] [--script_args SCRIPT_ARGS]
                    script

   Run a script in a docker on slurm. Accepts all slurm arguments to sbatch
   positional arguments:

    script                Script to execute
   optional arguments:

    -h, --help            show this help message and exit
    --docker_image DOCKER_IMAGE
                          Docker image (default: divkal/nvidia-pytorch:1.6)
    --workdir WORKDIR     Working directory (default: current directory)
    --docker_args DOCKER_ARGS
                          Extra docker arguments, to add with quotes
    --script_args SCRIPT_ARGS
                          Extra script arguments, to add with quotes
   ```
   

    
### Hard: How do I customly run batch scripts using Docker containers ? 
It's actually quite simple, let's say that you want to run a script named
`test_command.sh`, there are two ways of doing it: the easy one and the
original one.
    
1. Simple version: using the command `sdocker`. It's actually a command that
    I created for Titanic, and simplifies a bit the use of Nvidia-docker. Here's
    the command:
    
    ```bash
    sdocker -v volume_to_mount:where_to_mount dockerimage:version test_command 
    ```
    The command supports the nvidia-docker arguments. The arguments used here
    are: 
    - **-it**: used for an interactive docker session: useful to have terminal
      printouts and interaction.
    - **-v**: this command is actually quite important. It specifies the volumes
      that you want to mount into the container, because the container is like a
      virtual machine: you don't have access to the folders of the host machine.
    - **dockerimage:version** is the name of the container and its version that
      you want to mount.
    - **test_command.sh**: The command that you want to execute as soon as the
      container is running. Let's say that there's a script in the volume that
      you mounted. The `test_command` would be : 
      ```bash
      sdocker -v volume_to_mount:where_to_mount dockerimage:version \ 
            /bin/sh -c "cd where_to_mount/path_to_script; python script.py"
       ```
2. Original version
   As `sdocker` is a wrapper around `nvidia-docker`, the original command would
   be: 
   ```bash
    NV_GPU=$CUDA_VISIBLE_DEVICES exec nvidia-docker run --ipc=host --rm \
    -u=$(id -u):$(id -g) --init
    -v volume_to_mount:where_to_mount dockerimage:version test_command
   ```
  Check out the docs to note all the arguments. 

> Example:
>
> I have a script test.sh that I want to run, which is in /data/baltic_1/test/,
> and I need two GPU on pytorch for this job. So my command would be: 
>  ```bash
>  sbatch --gres=gpu:2 sdocker -v /data/baltic_1:/data \
>         divkal/nvidia-pytorch:1.1 /bin/sh -c "cd /data; ./test.sh"  
>  ```
3. CPU variant of `sdocker`: `sdocker` requires the user to have access to a
   GPU, as it uses nvidia-docker. **`sdocker-cpu`** is the said variant for CPU
   images. Also useful for debugging.
   ```bash
      sdocker-cpu -v volume_to_mount:where_to_mount dockerimage:version \ 
            /bin/sh -c "cd where_to_mount/path_to_script; python script.py"
   ```
   The original command is: 
   ```bash
      exec docker run --ipc=host --rm -u=$(id -u):$(id -g) --init
    -v volume_to_mount:where_to_mount dockerimage:version test_command
   ```
4. Slurm is unable to automatically kill the dockers when the job timeout or
preempt. In order to allow for slurm to kill them, add in your bash scripts the
following code block:
```bash
function dockerkill
{
    echo "Killing docker ${container_name}"
    docker kill $container_name
    echo "Cancelling job ${SLURM_JOB_ID}"
    scancel $SLURM_JOB_ID
}
trap dockerkill TERM
trap dockerkill INT
trap dockerkill CONT
```
where `$container_name` is a ID, which has to correspond to the 'sdocker' argument `'--name $container_name'`. 
  
### Interactive docker execution
The previous section described how to execute a bash script using docker. To
use a docker interactively, add the `-it` options to the `srun` command (that
asked for a pty). In the docker command, replace the script with the shell that
you want.

Example:

```bash
titanic-master$ srun --gres=gpu:1 --pty bash
...
compute-node$ sdocker -it docker_image /bin/bash
...
docker-container$ ... \# bash shell
```

### Do I need to create my own container ?
In most of the cases, no: many up-to-date libraries are already available in the
two docker containers already available:
1. [divkal/nvidia-pytorch:1.5](https://hub.docker.com/r/divkal/nvidia-pytorch/)
2. [divkal/nvidia-tensorflow:1.5](https://hub.docker.com/r/divkal/nvidia-tensorflow)

Follow the links to have more info on the configurations. 

### Yes, I really want to create my container ! 
Some package is missing, or you have a need for a special configuration: you
can create your own container. For space efficiency, **create your container
on top of other containers already on the nodes/upon dockerfiles already present
in this repository (in /dockerfiles),** unless necessary. This is really important for space
efficiency and not to store multiple times the same libs.

Check out the example Dockerfile and script to build and push in the
**/dockerfile-example** folder.
    
Also, check out the nice [Dockerfile tutorial
here](https://docs.docker.com/engine/reference/builder/#usage) to get more info
on how to create your container in a proper way.

### Contribute your Dockerfile to this repository
    Once you created your own docker container, please add your Dockerfile to
this repository (in /dockerfiles), under the format
`name_of_the_container:version.dockerfile`. This is good practice and will
allow for tractability of all the used docker containers. Please note that
in case of lack of space for dockers, not referenced containers could be
removed.

For those who did not create their Docker container using a Dockerfile, check
[here](https://stackoverflow.com/questions/19104847/how-to-generate-a-dockerfile-from-an-image) how to retrieve the Dockerfile from the image.  

> [Go to top](#the-titanic-cluster-documentation)

## Job monitoring

- You can first check your job state (WAITING, RUNNING, ...) using squeue.
- You can check node states using sinfo.
- To further debug/monitor/... for each launched job, you have an associated
  log file, (slurm-(job-id).out by default, can be specified with the options
  -e and -o, for stderr and stdout). Plain old logging in a specific log directory,
  on your personal storage, is still the preferred method when dealing with heavy
  logs.
- Checking the GPU consumption is a bit more tricky: when first running your
  job, note the GPU ID that was allocated to you by the SLURM daemon 
  (__ex: echo $CUDA_VISIBLE_DEVICES__) and the node. Then, run a small srun
  command on the node:

  ```bash
  srun -w titanic-1 --pty bash
  watch -n 5 nvidia-smi
  ```
  
- The **sstatus** or **sjob** command has been added to monitor your jobs more precisely, while
  giving info on the cluster. Here's an example: 

  ```bash
  dkalaina@titanic-master:~$ sjob
  Titanic status: 
  
  GPU Usage: 13/28
  Free Nodes: 3/7: titanic-[2,4-5]
  
  ==================================================
  Job details for user dkalaina:
  
  Using 4/4 GPUs allowed on priority queue.
  
   Partition        JobID    JobName      State      ReqGRES    CPUTime 
   ---------- ------------ ---------- ---------- ------------ ---------- 
     titanic 64190        exec_grid+    RUNNING        gpu:4 2-20:15:38
  ```
  
Note: In the future, a cluster monitoring solution (Ganglia) will be installed by INRIA engineers on the cluster.

> [Go to top](#the-titanic-cluster-documentation)

## Interactive sessions

- For debug purposes, you can use srun with the option --pty bash to launch an
  interactive session on the cluster. You can specify most of the previously
  mentionned options in your srun call. Be aware that if not specified, the time
  limit is set to 1 hour.
- Thanks to Victor Estrade, scripts are available to run jupyter notebooks directly
  on the cluster. Checkout the git for the scripts (available at https://gitlab.inria.fr/dkalaina/titanic-install) and documentation (or go directly
  to him).
- To do: Corentin's other solution + Cleanup code.
  
> [Go to top](#the-titanic-cluster-documentation)

## Bad practices

- Directly connecting to the cluster nodes (not titanic, but for example titanic-2).
- Changing $CUDA_VISIBLE_DEVICES, not using it, ...
- Using titanic-master as a computational node. This is not the way to go, and may
  break down he whole cluster if you put too much load on master, as it is a virtual
  machine, in charge of the nodes synchronization.
 
> [Go to top](#the-titanic-cluster-documentation)

## Reporting issues

- To report issues, use primarily the slack channel #titanic-users, or send a mail to tau-gpu@inria.fr.
- The users will be notified of the updates through the mailing list titanic-users@inria.fr

> [Go to top](#the-titanic-cluster-documentation)
